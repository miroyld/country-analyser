# Country Analyzer

This project is a Streamlit application for analyzing country data. It retrieves information such as the official name, capital, population, FIFA code, etc. for a selected country.

## Usage with streamlit
```
$ git clone https://gitlab.com/miroyld/country-analyser.git
$ cd country-analyzer
$ pip install -r requirements.txt
$ streamlit run src/main.py
```

## Usage with docker
```
$ git clone https://gitlab.com/miroyld/country-analyser.git
$ cd country-analyzer
$ docker build -t country-analyser .
$ docker run -p 8501:8501 country-analyser
```

Open your browser and go to http://localhost:8501 to view the application.
In the application, select a country from the dropdown to display the corresponding information.

## LICENCE MIT  
[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)  