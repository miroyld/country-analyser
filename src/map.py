import folium
from streamlit_folium import folium_static

class Map:
    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def show(self):
        m = folium.Map(location=[self.latitude, self.longitude], zoom_start=4)

        # Ajouter une couleur spécifique au pays sélectionné
        folium.CircleMarker(
            location=[self.latitude, self.longitude],
            radius=8,
            color='red',
            fill=True,
            fill_color='red'
        ).add_to(m)

        # Afficher la carte
        folium_static(m)
