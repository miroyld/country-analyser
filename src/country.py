import requests

class Country:
    def __init__(self, name, official_name, capital, population, fifa_code, latitude, longitude):
        self.name = name
        self.official_name = official_name
        self.capital = capital
        self.population = population
        self.fifa_code = fifa_code
        self.latitude = latitude
        self.longitude = longitude

    @classmethod
    def from_json(cls, json_data):
        data = json_data[0]

        name = data.get('name', {}).get('common', '')
        official_name = data.get('name', {}).get('official', '')
        capital = data.get('capital', [''])[0]
        population = data.get('population', 0)
        fifa_code = data.get('fifa', '')
        latitude, longitude = data.get('latlng', [None, None])

        return cls(name, official_name, capital, population, fifa_code, latitude=latitude, longitude=longitude)

def get_country(country):
    response = requests.get("https://restcountries.com/v3.1/name/" + country)
    json_data = response.json()

    if response.status_code == 200 and len(json_data) > 0:
        country_obj = Country.from_json(json_data)
        return country_obj
    else:
        return None

def get_flags():
    response = requests.get("https://restcountries.com/v3.1/all?fields=flags,name")
    countries_data = response.json()

    country_flags = {}

    for country_data in countries_data:
        country_name = country_data['name']['common']
        country_flags[country_name] = country_data['flags']['png']

    return country_flags
