import streamlit as st
from country import get_country, get_flags
from map import Map

st.set_page_config(
    page_title="Data analysis by country",
    page_icon="🌍"
)

hide_streamlit_style = """
<style>
footer {visibility: hidden;}
</style>
"""
st.markdown(hide_streamlit_style, unsafe_allow_html=True)

st.header('Data by country')

country_flags = get_flags()

country_names = list(country_flags.keys())

selected_country_name = st.selectbox("Select a country:", country_names)

country = get_country(selected_country_name)

if country:
    st.image(country_flags[selected_country_name], width=100)

    st.subheader("Country Information:")
    st.write("Country:", country.name)

    if country.official_name:
        st.write("Official name:", country.official_name)

    if country.capital:
        st.write("Capital city:", country.capital)

    if country.population:
        st.write("Population:", country.population)

    if country.fifa_code:
        st.write("FIFA Code:", country.fifa_code)

    st.subheader("Map:")
    if country.latitude and country.longitude:
        map_obj = Map(country.latitude, country.longitude)
        map_obj.show()

else:
    st.write("Unable to retrieve country information.")
