FROM python:3.8

WORKDIR /app

COPY src /app/src/
COPY requirements.txt /app/src/requirements.txt

RUN pip install -r src/requirements.txt

CMD ["streamlit", "run", "src/main.py"]

